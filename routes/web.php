<?php

use App\Http\Controllers\Admin\appointmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/dashboard');
});
Route::get('admin', function () {
    return redirect('admin/dashboard');
});

Route::get('/clear', function () {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    Cache::forget('spatie.permission.cache');
    return 'cleared';
});
Route::name('admin.')->group(function () {
    //LOGIN GROUP
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['guest:admin', 'throttle:100,1']], function () {
        //LOGIN ROUTE
        Route::get('login', ['as' => 'login.view', 'uses' => 'LoginController@getLogin']);
        Route::post('login', ['as' => 'login.view', 'uses' => 'LoginController@postLogin']);
    });

    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'auth:admin', 'throttle:100,1']], function () {
        Route::get('test', ['as' => 'test', 'uses' => 'TestController@getIndex']);

        //Route::get('newappointment', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@index']);

        //Route::get('appointment', ['as' => 'appointment.view', 'uses' => 'NewAppointmentController@index']);
        //my route
        //Route::get('newappointment', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@create']);
        //Route::post('newappointment', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@store'])->name('store');
        /*Route::get('newappointment/{id}', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@edit']);
        Route::put('newappointment/{id}', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@update']);
        Route::delete('newappointment/{id}', ['as' => 'newappointment.view', 'uses' => 'AppointmentsController@delete']);*/

        //Dashboard Route
        Route::get('dashboard', ['as' => 'dashboard.view', 'uses' => 'DashboardController@getIndex']);
        Route::get('profile', ['as' => 'dashboard.profile', 'uses' => 'DashboardController@getProfile']);
        Route::post('profile', ['as' => 'dashboard.profile', 'uses' => 'DashboardController@postProfile']);
        Route::get('password', ['as' => 'dashboard.password', 'uses' => 'DashboardController@getPassword']);
        Route::post('password', ['as' => 'dashboard.password', 'uses' => 'DashboardController@postPassword']);

        //Setting Route
        //        Route::get('settings', ['as' => 'settings.view', 'middleware' => ['permission:admin.settings.view'], 'uses' => 'SettingsController@getIndex']);
        //        Route::post('settings', ['as' => 'settings.view', 'middleware' => ['permission:admin.settings.view'], 'uses' => 'SettingsController@postIndex']);
        //
        //Users Route
        Route::get('users', ['as' => 'users.view', 'middleware' => ['permission:admin.users.view|admin.users.add|admin.users.edit|admin.users.delete|admin.users.password'], 'uses' => 'UsersController@getIndex']);
        Route::get('users/list', ['as' => 'users.list', 'middleware' => ['permission:admin.users.view|admin.users.add|admin.users.edit|admin.users.delete|admin.users.password'], 'uses' => 'UsersController@getList']);
        Route::get('users/add', ['as' => 'users.add', 'middleware' => ['permission:admin.users.add'], 'uses' => 'UsersController@getAdd']);
        Route::post('users/add', ['as' => 'users.add', 'middleware' => ['permission:admin.users.add'], 'uses' => 'UsersController@postAdd']);
        Route::get('users/edit/{id}', ['as' => 'users.edit', 'middleware' => ['permission:admin.users.edit'], 'uses' => 'UsersController@getEdit']);
        Route::post('users/edit/{id}', ['as' => 'users.edit', 'middleware' => ['permission:admin.users.edit'], 'uses' => 'UsersController@postEdit']);
        Route::get('users/password/{id}', ['as' => 'users.password', 'middleware' => ['permission:admin.users.password'], 'uses' => 'UsersController@getPassword']);
        Route::post('users/password/{id}', ['as' => 'users.password', 'middleware' => ['permission:admin.users.password'], 'uses' => 'UsersController@postPassword']);
        Route::get('users/delete/{id}', ['as' => 'users.delete', 'middleware' => ['permission:admin.users.delete'], 'uses' => 'UsersController@getDelete']);
        Route::get('users/permissions/{id}', ['as' => 'users.permissions', 'middleware' => ['permission:admin.users.permissions'], 'uses' => 'UsersController@getPermissions']);
        Route::post('users/permissions/{id}', ['as' => 'users.permissions', 'middleware' => ['permission:admin.users.permissions'], 'uses' => 'UsersController@postPermissions']);


        // ROLES ROUTE
        Route::get('roles', ['as' => 'roles.view', 'middleware' => ['permission:admin.roles.view|admin.roles.add|admin.roles.edit|admin.roles.delete|admin.roles.status|admin.roles.permissions'], 'uses' => 'RolesController@getIndex']);
        Route::get('roles/list', ['as' => 'roles.list', 'middleware' => ['permission:admin.roles.view|admin.roles.add|admin.roles.edit|admin.roles.delete|admin.roles.status|admin.roles.permissions'], 'uses' => 'RolesController@getList']);
        Route::get('roles/add', ['as' => 'roles.add', 'middleware' => ['permission:admin.roles.add'], 'uses' => 'RolesController@getAdd']);
        Route::post('roles/add', ['as' => 'roles.add', 'middleware' => ['permission:admin.roles.add'], 'uses' => 'RolesController@postAdd']);
        Route::get('roles/edit/{id}', ['as' => 'roles.edit', 'middleware' => ['permission:admin.roles.edit'], 'uses' => 'RolesController@getEdit']);
        Route::post('roles/edit/{id}', ['as' => 'roles.edit', 'middleware' => ['permission:admin.roles.edit'], 'uses' => 'RolesController@postEdit']);
        Route::get('roles/delete/{id}', ['as' => 'roles.delete', 'middleware' => ['permission:admin.roles.delete'], 'uses' => 'RolesController@getDelete']);
        Route::post('roles/status', ['as' => 'roles.status', 'middleware' => ['permission:admin.roles.status'], 'uses' => 'RolesController@postStatus']);
        Route::get('roles/permissions/{id}', ['as' => 'roles.permissions', 'middleware' => ['permission:admin.roles.permissions'], 'uses' => 'RolesController@getPermissions']);
        Route::post('roles/permissions/{id}', ['as' => 'roles.permissions', 'middleware' => ['permission:admin.roles.permissions'], 'uses' => 'RolesController@postPermissions']);


        // ROLES ROUTE
        /*
        Route::get('appointments', ['as' => 'appointments.view', 'uses' => 'AppointmentsController@getIndex']);
        Route::get('appointments/list', ['as' => 'appointments.list', 'uses' => 'AppointmentsController@getList']);
        Route::get('appointments/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@getAdd']);
        Route::post('appointments/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@postAdd']);
        Route::get('appointments/edit/{id}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@getEdit']);
        Route::post('appointments/edit/{id}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@postEdit']);
        Route::get('appointments/delete/{id}', ['as' => 'appointments.delete', 'uses' => 'AppointmentsController@getDelete']);
        Route::get('appointments/details/{id}', ['as' => 'appointments.details', 'uses' => 'AppointmentsController@getDelete']);
        Route::post('appointments/status', ['as' => 'appointments.status', 'uses' => 'AppointmentsController@getDetails']);*/

        Route::get('appointments', ['as' => 'appointments.view', 'middleware' => ['permission:admin.appointments.view|admin.appointments.add|admin.appointments.edit|admin.appointments.delete|admin.appointments.status'], 'uses' => 'AppointmentsController@getIndex']);
        Route::get('appointments/archived', ['as' => 'appointments.archived.view', 'middleware' => ['permission:admin.appointments.view|admin.appointments.add|admin.appointments.edit|admin.appointments.delete|admin.appointments.status'], 'uses' => 'AppointmentsController@getArchived']);
        Route::get('appointments/list', ['as' => 'appointments.list', 'middleware' => ['permission:admin.appointments.view|admin.appointments.add|admin.appointments.edit|admin.appointments.delete|admin.appointments.status'], 'uses' => 'AppointmentsController@getList']);
        Route::get('appointments/add', ['as' => 'appointments.add', 'middleware' => ['permission:admin.appointments.add'], 'uses' => 'AppointmentsController@getAdd']);
        Route::get('appointments/create', ['as' => 'appointments.add', 'middleware' => ['permission:admin.appointments.add'], 'uses' => 'AppointmentsController@create']);
        Route::post('appointments/add', ['as' => 'appointments.add', 'middleware' => ['permission:admin.appointments.add'], 'uses' => 'AppointmentsController@store']);
        Route::post('appointments/store', ['as' => 'appointments.add', 'middleware' => ['permission:admin.appointments.add'], 'uses' => 'AppointmentsController@store'])->name('store');;
        Route::get('appointments/edit/{id}', ['as' => 'appointments.edit', 'middleware' => ['permission:admin.appointments.edit'], 'uses' => 'AppointmentsController@edit'])->name('edit');
        Route::put('appointments/edit/{id}', ['as' => 'appointments.edit', 'middleware' => ['permission:admin.appointments.edit'], 'uses' => 'AppointmentsController@update']);
        Route::get('appointments/delete/{id}', ['as' => 'appointments.delete', 'middleware' => ['permission:admin.appointments.delete'], 'uses' => 'AppointmentsController@getDelete']);
        Route::get('appointments/details/{id}', ['as' => 'appointments.details', 'middleware' => ['permission:admin.appointments.details'], 'uses' => 'AppointmentsController@getDelete']);
        Route::post('appointments/status', ['as' => 'appointments.status', 'middleware' => ['permission:admin.appointments.status'], 'uses' => 'AppointmentsController@getDetails']);
        Route::post('appointments/update-status', ['as' => 'appointments.update_status', 'middleware' => ['permission:admin.appointments.status'],  'uses' => 'AppointmentsController@postUpdateStatus']);
        Route::post('appointments/archive/{id}', ['as' => 'appointments.archive', 'middleware' => ['permission:admin.appointments.edit'],  'uses' => 'AppointmentsController@postArchive']);
        Route::post('appointments/unarchive/{id}', ['as' => 'appointments.unarchive', 'middleware' => ['permission:admin.appointments.edit'],  'uses' => 'AppointmentsController@postUnArchive']);


//        Route::get('appointments/permissions/{id}', ['as' => 'appointments.permissions', 'middleware' => ['permission:admin.appointments.permissions'], 'uses' => 'RolesController@getPermissions']);
//        Route::post('appointments/permissions/{id}', ['as' => 'appointments.permissions', 'middleware' => ['permission:admin.appointments.permissions'], 'uses' => 'RolesController@postPermissions']);

/*
        // Appointments ROUTE
        Route::get('appointments', ['as' => 'appointments.view',  'uses' => 'AppointmentsController@getIndex']);
        Route::get('appointments/list', ['as' => 'appointments.list', 'uses' => 'AppointmentsController@getList']);
        Route::get('appointments/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@getAdd']);
        Route::post('appointments/add', ['as' => 'appointments.add', 'uses' => 'AppointmentsController@postAdd']);
        Route::get('appointments/edit/{id}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@getEdit']);
        Route::post('appointments/edit/{id}', ['as' => 'appointments.edit', 'uses' => 'AppointmentsController@postEdit']);
        Route::get('appointments/delete/{id}', ['as' => 'appointments.delete', 'uses' => 'AppointmentsController@getDelete']);
        Route::get('appointments/details/{id}', ['as' => 'appointments.details', 'uses' => 'AppointmentsController@getDetails']);
        Route::get('appointments/index', 'AppointmentsController@index')->name('index');
        Route::get('appointments/create', 'AppointmentsController@create')->name('create');
        Route::post('appointments/store', 'AppointmentsController@store')->name('store');
        Route::get('appointments/edit/{id}', 'AppointmentsController@edit')->name('edit');
        Route::put('appointments/update/{id}', 'AppointmentsController@update')->name('update');
        Route::delete('appointment/{id}','AppointmentsController@delete')->name('delete');
        Route::get('appointment/search','AppointmentsController@search')->name('search');
*/

        // Employees ROUTE
        Route::get('employees', ['as' => 'employees.view', 'middleware' => ['permission:admin.employees.view|admin.employees.add|admin.employees.edit|admin.employees.delete|admin.employees.status|admin.employees.permissions'], 'uses' => 'EmployeesController@getIndex']);
        Route::get('employees/list', ['as' => 'employees.list', 'middleware' => ['permission:admin.employees.view|admin.employees.add|admin.employees.edit|admin.employees.delete|admin.employees.status|admin.employees.permissions'], 'uses' => 'EmployeesController@getList']);
        Route::get('employees/add', ['as' => 'employees.add', 'middleware' => ['permission:admin.employees.add'], 'uses' => 'EmployeesController@getAdd']);
        Route::post('employees/add', ['as' => 'employees.add', 'middleware' => ['permission:admin.employees.add'], 'uses' => 'EmployeesController@postAdd']);
        Route::get('employees/edit/{id}', ['as' => 'employees.edit', 'middleware' => ['permission:admin.employees.edit'], 'uses' => 'EmployeesController@getEdit']);
        Route::post('employees/edit/{id}', ['as' => 'employees.edit', 'middleware' => ['permission:admin.employees.edit'], 'uses' => 'EmployeesController@postEdit']);
        Route::get('employees/delete/{id}', ['as' => 'employees.delete', 'middleware' => ['permission:admin.employees.delete'], 'uses' => 'EmployeesController@getDelete']);

        // BusinessSchedule ROUTE
        Route::get('business_schedules', ['as' => 'business_schedules.view', 'middleware' => ['permission:admin.business_schedules.view|admin.business_schedules.add|admin.business_schedules.edit|admin.business_schedules.delete|admin.business_schedules.status|admin.business_schedules.permissions'], 'uses' => 'BusinessScheduleController@getIndex']);
        Route::get('business_schedules/list', ['as' => 'business_schedules.list', 'middleware' => ['permission:admin.business_schedules.view|admin.business_schedules.add|admin.business_schedules.edit|admin.business_schedules.delete|admin.business_schedules.status|admin.business_schedules.permissions'], 'uses' => 'BusinessScheduleController@getList']);
        Route::get('business_schedules/add', ['as' => 'business_schedules.add', 'middleware' => ['permission:admin.business_schedules.add'], 'uses' => 'BusinessScheduleController@getAdd']);
        Route::post('business_schedules/add', ['as' => 'business_schedules.add', 'middleware' => ['permission:admin.business_schedules.add'], 'uses' => 'BusinessScheduleController@postAdd']);
        Route::get('business_schedules/edit/{id}', ['as' => 'business_schedules.edit', 'middleware' => ['permission:admin.business_schedules.edit'], 'uses' => 'BusinessScheduleController@getEdit']);
        Route::post('business_schedules/edit/{id}', ['as' => 'business_schedules.edit', 'middleware' => ['permission:admin.business_schedules.edit'], 'uses' => 'BusinessScheduleController@postEdit']);
        Route::get('business_schedules/delete/{id}', ['as' => 'business_schedules.delete', 'middleware' => ['permission:admin.business_schedules.delete'], 'uses' => 'BusinessScheduleController@getDelete']);
        Route::get('business_schedules/order', ['as' => 'business_schedules.order', 'middleware' => ['permission:admin.business_schedules.order'], 'uses' => 'BusinessScheduleController@getOrder']);
        Route::post('business_schedules/order', ['as' => 'business_schedules.order', 'middleware' => ['permission:admin.business_schedules.order'], 'uses' => 'BusinessScheduleController@postOrder']);
        Route::post('business_schedules/upload/{id?}', ['as' => 'business_schedules.upload', 'middleware' => ['permission:admin.business_schedules.order'], 'uses' => 'BusinessScheduleController@postUpload']);
        Route::post('business_schedules/upload-single/{id}', ['as' => 'business_schedules.upload_single', 'middleware' => ['permission:admin.business_schedules.order'], 'uses' => 'BusinessScheduleController@postUploadSingleImage']);
        Route::post('business_schedules/attachment/delete/{id}', ['as' => 'albums.delete_attachment', 'middleware' => ['permission:admin.business_schedules.order'] , 'uses' => 'BusinessScheduleController@postAttachmentDelete']);

        // Employees ROUTE
//        Route::get('employees', ['as' => 'employees.view',  'uses' => 'EmployeesController@getIndex']);
//        Route::get('employees/list', ['as' => 'employees.list', 'uses' => 'EmployeesController@getList']);
//        Route::get('employees/add', ['as' => 'employees.add', 'uses' => 'EmployeesController@getAdd']);
//        Route::post('employees/add', ['as' => 'employees.add', 'uses' => 'EmployeesController@postAdd']);
//        Route::get('employees/edit/{id}', ['as' => 'employees.edit', 'uses' => 'EmployeesController@getEdit']);
//        Route::post('employees/edit/{id}', ['as' => 'employees.edit', 'uses' => 'EmployeesController@postEdit']);
//        Route::get('employees/delete/{id}', ['as' => 'employees.delete', 'uses' => 'EmployeesController@getDelete']);
//        Route::get('employees/details/{id}', ['as' => 'employees.details', 'uses' => 'EmployeesController@getDetails']);
        // Tasks ROUTE
        Route::get('tasks', ['as' => 'tasks.view', 'middleware' => ['permission:admin.tasks.view'],  'uses' => 'TasksController@getIndex']);
        Route::get('tasks/archive', ['as' => 'tasks.archive', 'middleware' => ['permission:admin.tasks.view'],  'uses' => 'TasksController@getArchive']);
        Route::get('tasks/list', ['as' => 'tasks.list', 'middleware' => ['permission:admin.tasks.view'], 'uses' => 'TasksController@getList']);
        Route::get('tasks/list-archive', ['as' => 'tasks.list-archive', 'middleware' => ['permission:admin.tasks.archive'], 'uses' => 'TasksController@getArchiveList']);
        Route::get('tasks/add', ['as' => 'tasks.add', 'middleware' => ['permission:admin.tasks.add'], 'uses' => 'TasksController@getAdd']);
        Route::post('tasks/add', ['as' => 'tasks.add', 'middleware' => ['permission:admin.tasks.add'], 'uses' => 'TasksController@postAdd']);
        Route::get('tasks/edit/{id}', ['as' => 'tasks.edit', 'middleware' => ['permission:admin.tasks.edit'], 'uses' => 'TasksController@getEdit']);
        Route::post('tasks/edit/{id}', ['as' => 'tasks.edit', 'middleware' => ['permission:admin.tasks.edit'], 'uses' => 'TasksController@postEdit']);
        Route::get('tasks/delete/{id}', ['as' => 'tasks.delete', 'middleware' => ['permission:admin.tasks.delete'], 'uses' => 'TasksController@getDelete']);
        Route::get('tasks/details/{id}', ['as' => 'tasks.details', 'middleware' => ['permission:admin.tasks.details'], 'uses' => 'TasksController@getDetails']);
        Route::post('tasks/update-status', ['as' => 'tasks.update_status', 'middleware' => ['permission:admin.tasks.update_status'], 'uses' => 'TasksController@postUpdateStatus']);
        //Logout Route
        Route::get('logout', ['as' => 'dashboard.logout', 'uses' => 'DashboardController@getLogout']);
    });
});
//Route::get('admin/newappointment', 'Admin\AppointmentsController@index')->name('index');
//Route::post('admin/newappointment', 'Admin\AppointmentsController@store')->name('store');


//Dictionary Route
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('dictionary.js', function () {
        return response()->view('admin.common.general')->header('content-type', 'text/javascript; charset=utf-8');
    })->name('admin.common.general');
});




// pages
