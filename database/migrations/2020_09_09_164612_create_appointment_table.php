<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tittle', 255);
            $table->string('palce', 255);
            $table->string('type' , 3);
            $table->text('description')->nullable();
            $table->text('case')->nullable();
            $table->text('guests')->nullable();
            $table->date('date');
            $table->time('time_start')->unique();
            $table->time('end');
            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment');
    }
}
