<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create([
            'id' => 1,
            'name' => 'Admin',
            'guard_name' => 'admin',
            'status' => 1
        ]);
    }
}
