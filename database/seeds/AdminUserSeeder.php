<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'username' => 'admin',
            'name' => 'admin',
            'email' => 'admin@palwakf.net',
            'password' => '123456',
            'status' => 1,
        ]);
    }
}
