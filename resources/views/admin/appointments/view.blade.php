@extends('admin.layout.master')

@section('title')
    الرئيسية
@stop

@section('css')

@stop
@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i
                            class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.appointments.view') }}" class="kt-subheader__breadcrumbs-link">
                        المواعيد </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            البحث
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <input type="text" class="form-control searchable" id="name" name="name"
                                       placeholder="موضوع الزيارة">
                            </div>
                            <div class="col-lg-3">
                                <select id="period" class="form-control searchable">
                                    <option value="this_week">الأسبوع الحالي</option>
                                    <option value="today">اليوم</option>
                                    <option value="this_month">الشهر الحالي</option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control searchable date-picker" id="from" name="from"
                                       placeholder="التاريخ من">
                            </div>
                            <div class="col-lg-3">
                                <input type="text" class="form-control searchable date-picker" id="to" name="to"
                                       placeholder="إلى">
                            </div>
                            <button class="search" style="display: none"></button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
                <input type="hidden" id="archived" name="archived" value="{{ $archived }}" >
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->

    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    إدارة المواعيد
                </h3>
            </div>
            @can('admin.appointments.add')
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ route('admin.appointments.add') }}"
                               class="btn btn-brand btn-elevate btn-icon-sm">
                                إضافة جديد
                            </a>

                        </div>
                    </div>
                </div>
            @endcan
        </div>
        <div class="kt-portlet__body">
            <div class="row mb20">
                <table class="table-bordered" style="border:1px solid #ccc !important;color: #000; margin-bottom: 20px" width="100%" align="center">
                    <tbody><tr style="height: 30px; text-align: center;">
                        <td width="20%" style="background-color: #A4D6A5;">الأحد</td>
                        <td width="20%" style="background-color: #FFCC88;">الإثنين</td>
                        <td width="20%" style="background-color: #FFFF99;">الثلاثاء</td>
                        <td width="20%" style="background-color: #B2DFF6;">الأربعاء</td>
                        <td width="20%" style="background-color: #F7B3B4;">الخميس</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover" id="appointments_table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>اليوم والتاريخ</th>
                    <th>الساعة من</th>
                    <th>الساعة إلى</th>
                    <th>الضيوف</th>
                    <th>موضوع الزيارة</th>
                    <th>الجهة المشاركة</th>
                    <th>الاعتماد</th>
{{--                    <th></th>--}}
                    <th>أدوات</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/appointments.js?v=2" type="text/javascript"></script>
    <script>

        $('.date-picker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
        $('#period').on('change', function () {
            let period = $(this).val();
            if(period == 'today'){
                let today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();
                today = yyyy + '-' + mm + '-' + dd;
                $('#from').val(today);
                $('#to').val(today);
            }
            else if(period == 'this_week'){
                var curr = new Date(); // get current date
                var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6

                var firstday = new Date(curr.setDate(first));
                var _lastday = new Date(curr.setDate(last));

                var dd = String(firstday.getDate()).padStart(2, '0');
                var mm = String(firstday.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = firstday.getFullYear();
                var first_day = yyyy + '-' + mm + '-' + dd;
                $('#from').val(first_day);

                var dd = String(_lastday.getDate()).padStart(2, '0');
                var mm = String(_lastday.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = _lastday.getFullYear();
                var last_day = yyyy + '-' + mm + '-' + dd;
                $('#to').val(last_day);
            }
            else if(period == 'this_month'){
                var curr = new Date(); // get current date
                var dd = 1;
                var mm = String(curr.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = curr.getFullYear();
                var first_day = yyyy + '-' + mm + '-' + dd;
                $('#from').val(first_day);

                var last = new Date(yyyy, mm, dd);
                last.setDate(last.getDate() - 1);
                var dd = String(last.getDate()).padStart(2, '0');
                var mm = String(last.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = last.getFullYear();
                var last_day = yyyy + '-' + mm + '-' + dd;
                $('#to').val(last_day);

            }
            //$('.search').click();
        });
        $('#period').change();
    </script>
@stop
