@extends('admin.layout.master')

@section('title')
    موعد جديد
@stop

@section('css')
    <link rel="stylesheet" href="assets/admin/css/tagify.css">
    <style>
        .bootstrap-timepicker-widget,
        #time_start,#time_end{
            direction: ltr;
        }
    </style>
@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        الرئيسية </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        موعد جديد </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            موعد جديد
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" method="post" action="{{ route('admin.appointments.edit' , [$appointment->id]) }}">
                    @csrf
                    <input type="hidden" name="_method" value="put">

                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label for= "tittle">عنوان الموعد</label>
                                <input autocomplete="off" type="text" name="tittle" class="form-control searchable @error('tittle') is-invalid @enderror " id="tittle" placeholder="عنوان الموعد" value="{{ old('tittle', $appointment->tittle ) }}" >
                                @error('tittle')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-lg-6">
                                <label for="date">التاريخ</label>
                                <input autocomplete="off"
                                    class="form-control date @error('date') is-invalid @enderror"
                                    name="date"
                                    id="date"
                                    autocomplete="off"
                                    value="{{ old('date', $appointment->date ) }}"
                                />
                                @error('date')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        @if(false)
                            <div class="col-lg-6">
                                <label for= "palce">مكان اللقاء</label>
                                <input autocomplete="off" type="text" name="palce" class="form-control searchable @error('palce') is-invalid @enderror" id="palce" placeholder="مكان الللقاء" value="{{ old('palce', $appointment->palce ) }}">
                                @error('palce')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        @endif
                        </div>
                        @if(false)
                        <div class="form-group row">
{{--                            <div class="col-lg-6">--}}
{{--                                <label for="type">نوع اللقاء</label>--}}
{{--                                <select id="type" name="type" class="form-control kt-selectpicker searchable @error('type') is-invalid @enderror "  data-live-search="true">--}}
{{--                                    <option value="" disabled selected>نوع اللقاء</option>--}}
{{--                                    <option value="عام" @if($appointment->type == 'عام') selected @endif >عام</option>--}}
{{--                                    <option value="خاص" @if($appointment->type == 'خاص') selected @endif> خاص </option>--}}
{{--                                </select>--}}
{{--                                @error('type')--}}
{{--                                <p class="text-danger">{{ $message }}</p>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
                            <div class="col-lg-6" style="display: none">
                                <label for="color">اللون</label>
                                <input autocomplete="off"
                                       class="form-control @error('date') is-invalid @enderror"
                                       name="color"
                                       type="color"
                                       value="{{ old('color', $appointment->color ) }}"
                                />
                                @error('date')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">

                            <div class="col-lg-6">
                                <label for="time_start"> الوقت من</label>
                                <input autocomplete="off"
                                    type="text"
                                    class="form-control @error('time_start') is-invalid @enderror "
                                    name="time_start"
                                    id="time_start"
                                    autocomplete="off"
                                    value="{{ old('time_start', $appointment->time_start ) }}"

                                />
                                @error('time_start')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-lg-6">
                                <label for="time_end">الوقت إلى</label>
                                <input autocomplete="off"
                                    type="text"
                                    class="form-control @error('time_end') is-invalid @enderror  "
                                    name="time_end"
                                    id="time_end"
                                    autocomplete="off"
                                    value="{{ old('time_end', $appointment->time_end ) }}"

                                />
                                @error('time_end')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <label for="description">وصف عام للنقاش</label>--}}
{{--                                <textarea autocomplete="off" name="description" class="form-control @error('time_end') is-invalid @enderror" id="description" placeholder="وصف عام للنقاش">{{ old('description', $appointment->description ) }}</textarea>--}}
{{--                                @error('description')--}}
{{--                                <p class="text-danger">{{ $message }}</p>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-6">--}}
{{--                                <label for="Issues">قضايا مطروحة</label>--}}
{{--                                <textarea name="Issues" class="form-control" id="Issues" placeholder="قضايا مطروحة">{{ old('Issues', $appointment->Issues ) }}</textarea>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-group row">
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <label for="guests">الضيوف</label>
                                <input id="kt_tagify_5" name='guests' placeholder="إضافة ضيف" value="{{ old('guests', $appointment->guests ) }}" >

                            </div>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <label for="participant">الجهة المشاركة</label>
                                <input id="participant" class="form-control" name='participant' placeholder="الجهة المشاركة" value="{{ old('guests', $appointment->participant ) }}" >
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3">

                                <button type="submit" class="btn btn-brand btn-elevate btn-icon-sm"> حفظ التعديل </button>
                            </div>

                        </div>


                    </div>
                </form>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->

@stop

@section('js')
    <script>

        var toEl = document.getElementById('kt_tagify_5');
        var tagifyTo = new Tagify(toEl, {
            delimiters: ", ", // add new tags when a comma or a space character is entered
            maxTags: 10,
            keepInvalidTags: true, // do not remove invalid tags (but keep them marked as invalid)
            templates: {
                dropdownItem : function(tagData){
                    try{
                        return '<div class="tagify__dropdown__item">' +
                            '<div class="kt-media-card">' +
                            '    <span class="kt-media kt-media--'+(tagData.initialsState?tagData.initialsState:'')+'" style="background-image: url('+tagData.pic+')">' +
                            '        <span>'+tagData.initials+'</span>' +
                            '    </span>' +
                            '    <div class="kt-media-card__info">' +
                            '        <a href="#" class="kt-media-card__title">'+tagData.value+'</a>' +
                            '        <span class="kt-media-card__desc">'+tagData.email+'</span>' +
                            '    </div>' +
                            '</div>' +
                            '</div>';
                    }
                    catch(err){}
                }
            },
            transformTag: function(tagData) {
                tagData.class = 'tagify__tag tagify__tag--brand';
            },
            dropdown : {
                classname : "color-blue",
                enabled   : 1,
                maxItems  : 5
            }
        });
        $('#time_start,#time_end').timepicker({
            template: 'dropdown',
            minuteStep: 15,
            defaultTime: '08:00',
            showSeconds: false,
            showMeridian: true,
            snapToStep: true,
            showInputs: true,
            disableMousewheel: false
        });
    </script>

@stop
