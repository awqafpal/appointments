@can('admin.appointments.status')
    <select class="form-control appointment-status" data-id="{{ $id_hash }}">
        <option value="0" {{ $status == 0 ? 'selected' : '' }}>غير معتمد</option>
        <option value="1" {{ $status == 1 ? 'selected' : '' }}>معتمد</option>
    </select>
@else
    @if($status == 0)
        <span class="btn-sm btn btn-label-danger">غير معتمد</span>
    @else
        <span class="btn-sm btn btn-label-success">معتمد</span>
    @endif
@endcan
