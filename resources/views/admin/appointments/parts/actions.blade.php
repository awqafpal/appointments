@if($archived)
    <a href="javascript:;" data-url="{{ route('admin.appointments.unarchive', ['id' => $id_hash]) }}"
       class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm archive_btn" title="إلغاء الأرشفة">
        <i class="fas fa-undo"></i>
    </a>
@else
    <a href="javascript:;" data-url="{{ route('admin.appointments.archive', ['id' => $id_hash]) }}"
       class="btn btn-outline-info btn-elevate-hover btn-circle btn-icon btn-sm archive_btn" title="أرشفة">
        <i class="fas fa-archive"></i>
    </a>
@endif
<a href="{{ route('admin.appointments.edit', ['id' => $id_hash]) }}"
   class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
    <i class="la la-edit"></i>
</a>
<a href="javascript:;" data-url="{{ route('admin.appointments.delete', ['id' => $id_hash]) }}"
   class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
    <i class="la la-trash"></i>
</a>
