@if(auth()->user()->can('admin.tasks.edit') || auth()->user()->can('admin.tasks.password') || auth()->user()->can('admin.tasks.delete'))
   @can('admin.tasks.edit')
        <a href="{{ route('admin.tasks.edit', ['id' => $id_hash]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
    @endcan
    @can('admin.tasks.delete')
        <a href="javascript:;" data-url="{{ route('admin.tasks.delete', ['id' => $id_hash]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
    @endcan
@endif
