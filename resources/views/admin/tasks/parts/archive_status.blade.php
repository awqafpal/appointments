@if($status != 'done')
    <span class="btn-sm btn btn-label-danger">{{ $task_status[$status] }}</span>
@else
    <span class="btn-sm btn btn-label-success">{{ $task_status[$status] }}</span>
@endif
