@extends('admin.layout.master')

@section('title')
    موعد جديد
@stop

@section('css')
<link rel="stylesheet" href="assets/admin/css/tagify.css">
{{-- <style>
    .tagify {
        direction: ltr
    }
</style> --}}
@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        الرئيسية </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="" class="kt-subheader__breadcrumbs-link">
                        موعد جديد </a>
                </div>
            </div>
        </div>
    </div>
@stop

    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    إدارة المواعيد
                </h3>
            </div>
            @can('admin.users.add')
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">

                        </div>
                    </div>
                </div>
            @endcan
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover" id="users_table">
                <thead>

                    <tr>
                    <th>#</th>
                    <th>العنوان</th>
                    <th>المكان</th>
                    <th>نوع اللقاء</th>
                    <th>التاريخ</th>
                    <th>الحالة</th>
                    <th>أدوات</th>
                </tr>

                </thead>
                <tbody>
                {{--<?php foreach ($appointment as $appoint ) : ?>--}}


                    <tr>
                        <td> 1 </td>
                        <td>عنوان 1</td>
                        <td>مكان 1</td>
                        <td>نوع 1</td>
                        <td>تاريخ 1</td>
                        <td>حالة 1</td>
                        <td>
                            <a href="#" class="btn btn-sm btn-success btn-icon btn-icon-md" title="View">
                                <i class="la la-edit"></i>
                              </a>
                              <a href="#" class="btn btn-sm btn-info btn-icon btn-icon-md" title="View">
                                <i class="la la-eye"></i>
                              </a>
                              <a href="#" class="btn btn-sm btn-danger btn-icon btn-icon-md" title="View">
                                <i class="la la-trash"></i>
                              </a>
                        </td>
                    </tr>
                   {{--<?php endforeach ?>--}}
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script>

        var toEl = document.getElementById('kt_tagify_5');
        var tagifyTo = new Tagify(toEl, {
            delimiters: ", ", // add new tags when a comma or a space character is entered
            maxTags: 10,
            blacklist: ["fuck", "shit", "pussy"],
            keepInvalidTags: true, // do not remove invalid tags (but keep them marked as invalid)
            whitelist: [
                {
                value : 'محمد خالد',
                email : 'chris.muller@wix.com',
                initials: '',
                initialsState: '',
                pic: './assets/media/users/100_11.jpg',
                class : 'tagify__tag--brand'
            }, ],
            templates: {
                dropdownItem : function(tagData){
                    try{
                        return '<div class="tagify__dropdown__item">' +
                            '<div class="kt-media-card">' +
                            '    <span class="kt-media kt-media--'+(tagData.initialsState?tagData.initialsState:'')+'" style="background-image: url('+tagData.pic+')">' +
                            '        <span>'+tagData.initials+'</span>' +
                            '    </span>' +
                            '    <div class="kt-media-card__info">' +
                            '        <a href="#" class="kt-media-card__title">'+tagData.value+'</a>' +
                            '        <span class="kt-media-card__desc">'+tagData.email+'</span>' +
                            '    </div>' +
                            '</div>' +
                            '</div>';
                    }
                    catch(err){}
                }
            },
            transformTag: function(tagData) {
                tagData.class = 'tagify__tag tagify__tag--brand';
            },
            dropdown : {
                classname : "color-blue",
                enabled   : 1,
                maxItems  : 5
            }
        });
    </script>
@stop
