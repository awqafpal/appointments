@extends('admin.layout.master')

@section('title')
    إضافة بند جدول الأعمل
@stop

@section('css')

@stop

@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.business_schedules.view') }}" class="kt-subheader__breadcrumbs-link">
                        جدول الأعمال
                    </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.business_schedules.add') }}" class="kt-subheader__breadcrumbs-link">
                        إضافة بند
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- begin:: Content -->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            إضافة بند
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="kt-form kt-form--label-right" id="frmAdd" method="post" action="{{ route('admin.business_schedules.add') }}">
                    <input type="hidden" id="business-schedules-id">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label class="">البند:</label>
                                <textarea autocomplete="off" type="text" class="form-control" name="title" placeholder="البند">{{ old('title') }}</textarea>
                            </div>
                            <div class="col-lg-6">
                                <label class="">جهة التنسيب:</label>
                                <input autocomplete="off" type="text" class="form-control" name="placement_party" value="{{ old('placement_party') }}" placeholder="جهة التنسيب">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">مشروع القرار المقترح:</label>
                                <textarea autocomplete="off" type="text" class="form-control" name="project" value="{{ old('project') }}" placeholder="مشروع القرار المقترح" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">ملاحظات :</label>
                                <textarea autocomplete="off" type="text" class="form-control" name="notes" value="{{ old('notes') }}" placeholder="ملاحظات " rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group row" style="display: none">
                            <div class="col-lg-6">
                                <label class="">الحالة:</label>
                                <select class="form-control kt-selectpicker" name="status" data-live-search="true">
                                    <option value="1">فعال</option>
                                    <option value="0">معطل</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">المرفقات</label>
                            <div class="col-lg-12">
                                <div class="dropzone dropzone-default dropzone-success" id="my-dropzone">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">يمكنك سحب الملفات أو الضغط للإضافة</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label class="">معاينة المرفقات</label>
                                <div class="images-list"></div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6 kt-align-right">
                                    <button type="submit" class="btn btn-brand save btnSave">حفظ</button>
                                    <a href="{{ route('admin.business_schedules.view') }}" class="btn btn-secondary">الغاء</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <!-- End:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/business_schedules.js" type="text/javascript"></script>
    <script>

        var first_item = true;
        function showNotification(title, message, type) {

            var notify = $.notify({
                'title': title,
                'message': message,
            }, {

                element: 'body',
                position: null,
                type: type,
                allow_dismiss: true,
                newest_on_top: true,
                showProgressbar: false,
                placement: {
                    from: "bottom",
                    align: Common.getCurrentLanguage() === 'ar' ? "left" : "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 5000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class',
            });
        }
        var myDropzone = $('#my-dropzone').dropzone({
            url: "/",
            acceptedFiles: "image/*,.pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Accept-Language': Common.getCurrentLanguage() === 'ar' ? "ar" : "en"
            },
            parallelUploads: 30,
            init: function () {
                var submitButton = document.querySelector(".btnSave")
                var myDropzone = this;
                submitButton.addEventListener("click", function (e) {
                    //     // Make sure that the form isn't actually being sent.
                    e.preventDefault();
                    e.stopPropagation();

                    // App.blockUI({
                    //     target: '.form-actions',
                    //     iconOnly: true
                    // });
                    var form = $('form');
                    var link = $(form).attr('action');
                    var data = $(form).serialize();
                    var method = $(form).attr('method');

                    var flag = (typeof (data) === 'string') ? 'application/x-www-form-urlencoded; charset=UTF-8' : false;
                    $.ajax({
                        type: method,
                        url: link,
                        data: data,
                        dataType: 'json',
                        processData: flag,
                        contentType: flag,
                    }).done(function (data) {
                        if (data.status == "true") {
                            $('.btnSave').prop('disabled', true);
                            $('.kt-form__actions').hide();
                            $('#business-schedules-id').val(data.data.id);
                            myDropzone.options.url = 'admin/business_schedules/upload/' + data.data.id;
                            myDropzone.processQueue();
                        } else {
                            showNotification('{{ trans('title.error') }}', data.message, 'danger');
                        }
                    });
                });
                this.on("queuecomplete", function (data) {
                    showNotification('{{ trans('title.success') }}', '{{ trans('messages.success') }}', 'success');
                });
                this.on("success", function (data) {
                    if(first_item){
                        var json_data = JSON.parse(data.xhr.response).data;
                        var i;
                        for (i = 0; i < json_data.length; i++) {
                            var id = json_data[i].id;
                            var url = json_data[i].url;
                            $('.images-list').append('<div class="media-item " ><a class="remove" href="{{ asset('admin/business_schedules/attachment/delete') }}/'+ id + '" data-id="' + id + '">X</a><div class="img"><img src="' + url + '"></div><div><a target="_blank" href="' + url + '">عرض</a></div></div>');
                        }
                        first_item = false;
                    }
                });
            }
        });
        $(document).on('click', '.media-item a.remove', function () {
            var item = $(this);
            var id = $(this).data('id');
            Common.confirm(function () {
                $.ajax({
                    type: 'post',
                    url: item.attr('href'),
                    data: {id: id, business_schedules_id: $('#business-schedules-id').val()},
                    dataType: 'json',
                }).done(function (data) {
                    if (data.status == "true") {
                        showNotification('{{ trans('title.success') }}', '{{ trans('messages.success') }}', 'success');
                        item.parent().remove();
                    } else {
                        showNotification('{{ trans('title.error') }}', '{{ trans('messages.error') }}', 'danger');
                    }
                });
            });
            return false;
        });
    </script>
@stop
