{{--@if(auth()->user()->can('admin.business_schedules.edit') || auth()->user()->can('admin.business_schedules.password') || auth()->user()->can('admin.business_schedules.delete'))--}}
{{--   @can('admin.business_schedules.edit')--}}
        <a href="{{ route('admin.business_schedules.edit', ['id' => $id_hash]) }}" class="btn btn-outline-success btn-elevate-hover btn-circle btn-icon btn-sm" title="تعديل">
            <i class="la la-edit"></i>
        </a>
{{--    @endcan--}}
{{--    @can('admin.business_schedules.delete')--}}
        <a href="javascript:;" data-url="{{ route('admin.business_schedules.delete', ['id' => $id_hash]) }}" class="btn btn-outline-danger btn-elevate-hover btn-circle btn-icon btn-sm delete_btn" title="حذف">
            <i class="la la-trash"></i>
        </a>
{{--    @endcan--}}
{{--@endif--}}
