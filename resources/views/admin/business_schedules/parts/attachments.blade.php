@if($attachments != null)
    <ul style="padding: 0; list-style: none">
    @foreach($attachments as  $k => $item)
            <li>
                <a href="{{ asset($item->name) }}" target="_blank">
                    مرفق {{ $k + 1 }}
                </a>
            </li>
    @endforeach
    </ul>
@endif
