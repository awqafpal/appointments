@extends('admin.layout.master')

@section('title')
    الرئيسية
@stop
@section('css')
    <!--<link href="assets/admin/plugins/custom/jquery-ui/jquery-ui.bundle.rtl.css" rel="stylesheet" type="text/css"/>-->
    <style>
        .business_schedule_items {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .business_schedule_items li {
            margin: 0 3px 3px 3px;
            padding: 0.4em;
            padding-left: 1.5em;
            font-size: 1.4em;
            border: 1px solid #c5c5c5;
            background: #f6f6f6;
            font-weight: normal;
            color: #454545;
        }

        .business_schedule_items li:hover {
            cursor: pointer;
        }
    </style>
@stop
@section('subheader')
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    وزارة الأوقاف والشؤون الدينية </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-home"><i
                            class="flaticon2-shelter"></i></a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.dashboard.view') }}" class="kt-subheader__breadcrumbs-link">
                        الرئيسية </a>
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                    <a href="{{ route('admin.business_schedules.view') }}" class="kt-subheader__breadcrumbs-link">
                        ترتيب جدول الأعمال
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')

    <!-- begin:: Content -->
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    إدارة جدول الأعمال
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row mb-3">
            </div>

            <div class="kt-portlet__head-toolbar mb-3">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button class="btn btn-success  btn-icon-sm btn-save-order">
                            حفظ الترتيب
                        </button>
                    </div>
                </div>
            </div>
            <ul class="business_schedule_items mb-3">
                @foreach($business_schedule_items as $k => $item)
                    <li data-id="{{ $item->id }}">{{ $item->title }}</li>
                @endforeach
            </ul>
            <div class="kt-portlet__head-toolbar ">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button class="btn btn-success  btn-icon-sm btn-save-order">
                            حفظ الترتيب
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@stop

@section('js')
    <script src="assets/admin/general/js/scripts/business_schedules.js" type="text/javascript"></script>
    <script src="assets/admin/plugins/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>
    <script src="assets/admin/plugins/custom/jquery-ui/jquery.ui.touch-punch.js" type="text/javascript"></script>
    <script>
        $('.business_schedule_items').sortable();
        $('.btn-save-order').on('click', function (e){
            var ids = Array();
            $('.business_schedule_items li').each(function (){
                ids.push($(this).data('id'));
            });
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.business_schedules.order') }}',
                data: { ids: ids},
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            }).done(function (data) {
                if(data.code == 200){
                    toastr['success'](data.message);
                }
                // notify(data.message, getNotificationType(data.code));
            }).fail(function (request, status, error) {
                var message = "(";
                message += request.status;
                message += ") ";
                message += request.statusText;
                // notify(message, getNotificationType(500));
                notify('أوه!', getNotificationType(500), 'عذراً - حدث خطأ أثناء معالجة البيانات');

                if(parseInt(request.status) === 401) {
                    var delay = 1750;

                    setTimeout(function() {
                        window.location = base_url;
                    }, delay);
                }
            });
        });
    </script>
@stop
