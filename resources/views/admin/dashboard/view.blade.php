@extends('admin.layout.master')

@section('title')
    الرئيسية
@stop

@section('css')

@stop

@section('subheader')

@stop


@section('content')
    <style>
        .svg-icon-white svg path,
        .svg-icon-white svg rect{
            fill: #fff;
            stroke: #fff;
        }
        .white-text{
            color: #fff;
        }
        .font-size-h2{
            font-size: 26px;
        }
    </style>
    <div class="row">
        @can('admin.appointments.view')
            <div class="col-xl-4">
                <!--begin::Stats Widget 14-->
                <a href="{{ route('admin.appointments.view') }}" class="card card-custom bg-success bg-hover-state-success card-stretch gutter-b">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="text-inverse-primary font-weight-bolder font-size-h2 mb-2 mt-5 white-text">المواعيد</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 14-->
            </div>
        @endcan
{{--        @can('admin.employees.view')--}}
{{--            <div class="col-xl-4">--}}
{{--                <!--begin::Stats Widget 14-->--}}
{{--                <a href="{{ route('admin.employees.view') }}" class="card card-custom bg-primary bg-hover-state-primary card-stretch gutter-b">--}}
{{--                    <!--begin::Body-->--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="text-inverse-primary font-weight-bolder font-size-h2 mb-2 mt-5 white-text">الموظفين</div>--}}
{{--                    </div>--}}
{{--                    <!--end::Body-->--}}
{{--                </a>--}}
{{--                <!--end::Stats Widget 14-->--}}
{{--            </div>--}}
{{--        @endcan--}}
        @can('admin.tasks.view')
            <div class="col-xl-4">
                <!--begin::Stats Widget 14-->
                <a href="{{ route('admin.tasks.view') }}" class="card card-custom bg-info bg-hover-state-info card-stretch gutter-b">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="text-inverse-primary font-weight-bolder font-size-h2 mb-2 mt-5 white-text">المهام</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 14-->
            </div>
        @endcan
        @can('admin.business_schedules.view')
            <div class="col-xl-4">
                <!--begin::Stats Widget 14-->
                <a href="{{ route('admin.business_schedules.view') }}" class="card card-custom bg-danger bg-hover-state-danger card-stretch gutter-b">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="text-inverse-primary font-weight-bolder font-size-h2 mb-2 mt-5 white-text">جدول الأعمال</div>
                    </div>
                    <!--end::Body-->
                </a>
                <!--end::Stats Widget 14-->
            </div>
        @endcan
    </div>
@stop

@section('js')

@stop
