var Appointments = function () {
    var view_tbl;
    var view_url = base_url + prefix + '/appointments';
    var list_url = base_url + prefix + '/appointments/list';
    var table_id = '#appointments_table';
    /////////////////// View //////////////////
    ///////////////////////////////////////////
    var viewTable = function () {
        var link = list_url;
        var columns = [
            {"data": "DT_RowIndex", "title": "#", "orderable": false, "searchable": false},
            {"data": "date", "orderable": true, "searchable": true},
            {"data": "time_start", "orderable": true, "searchable": true},
            {"data": "time_end", "orderable": true, "searchable": true},
            {"data": function ( data, type, row ) {
                var guests = JSON.parse(data['guests']);
                var guests_output = '';
                for(var i in guests){
                    guests_output += guests[i].value + '<false>';
                }
                return guests_output;
            },
                "orderable": true, "searchable": true},
            {"data": "tittle", "orderable": true, "searchable": true},
            {"data": "participant", "orderable": true, "searchable": true},
            {"data": "status", "orderable": true, "searchable": true},
            // {"data": function ( data, type, row ) {
            //         return '<div style="background-color:' + data['color'] +'; height: 38px" ></div>';
            //     },
            //     "orderable": true, "searchable": false},
            {"data": "actions", "orderable": false, "searchable": false, "class" : "text-center"}
        ];
        var perPage = 25;
        var order = [
            [1, 'asc'],
            [2, 'asc'],
        ];

        var ajaxFilter = function (d) {
            d.name = $('#name').val();
            d.from = $('#from').val();
            d.to = $('#to').val();
            d.archived = $('#archived').val();
        };

        view_tbl = DataTable.init($(table_id), link, columns, order, ajaxFilter, perPage, false);
    };
    /////////////////// ADD ///////////////////
    ///////////////////////////////////////////
    var add = function () {
        $('#frmAdd').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');

            Forms.doAction(link, formData, method, null, addCallBack);
        });
    };

    var addCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////////////// EDIT //////////////////
    ///////////////////////////////////////////
    var edit = function () {
        $('#frmEdit').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, editCallBack);
        });
    };

    var editCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////// Change Password ///////////////
    ///////////////////////////////////////////
    var changePassword = function () {
        $('#frmChangePassword').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, changePasswordCallBack);
        });
    };

    var changePasswordCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    /////////// Role Permissions ///////////////
    ///////////////////////////////////////////
    var permissions = function () {
        $('#frmPermissions').submit(function(e) {
            e.preventDefault();
            var link = $(this).attr('action');
            var formData = $(this).serialize();
            var method = $(this).attr('method');
            Forms.doAction(link, formData, method, null, permissionsCallBack);
        });
    };

    var permissionsCallBack = function (obj) {
        if(obj.code === 200) {
            var delay = 1750;

            setTimeout(function () {
                window.location = view_url;
            }, delay);
        }
    };
    //////////////// DELETE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "GET";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// ARCHIVE ///////////////////
    ///////////////////////////////////////////
    var deleteItem = function () {
        $(document).on('click', '.archive_btn', function (e) {
            e.preventDefault();
            var btn = $(this);

            Common.confirm(function() {
                var link = btn.data('url');
                var formData = {};
                var method = "POST";

                Forms.doAction(link, formData, method, view_tbl);
            });
        });
    };
    //////////////// Search ///////////////////
    ///////////////////////////////////////////
    var search = function () {
        $('.searchable').on('input change', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('.search').on('click', function (e) {
            e.preventDefault();
            view_tbl.draw(false);
        });

        $('#frmSearch').keydown(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                view_tbl.draw(false);
            }
        });
    };
    //////////////// UPDATE STATUS ///////////////////
    ///////////////////////////////////////////
    var UpdateItemStatus = function () {
        $(document).on('change', '.appointment-status', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var status = $(this).val();
            var link = view_url + '/update-status';
            var formData = new FormData();
            formData.append('id', id);
            formData.append('status', status);
            var method = "POST";

            Forms.doAction(link, formData, method, view_tbl);
        });
    };
    ///////////////// INITIALIZE //////////////
    ///////////////////////////////////////////
    return {
        init: function () {
            viewTable();
            add();
            edit();
            changePassword();
            permissions();
            deleteItem();
            search();
            UpdateItemStatus();
        }
    }
}();

$(document).ready(function() {
    Appointments.init();
});
