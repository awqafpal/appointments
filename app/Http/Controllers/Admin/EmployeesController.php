<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FaqController
 * @property EmployeeRepositoryInterface $employee_repo
 * @package App\Http\Controllers\Admin
 */
class EmployeesController extends AdminController
{
    /**
     * @var EmployeeRepositoryInterface
     */
    protected $employee_repo;
    /**
     * FaqController constructor.
     * @param EmployeeRepositoryInterface $employee_repo
     */
    public function __construct(EmployeeRepositoryInterface $employee_repo)
    {
        parent::__construct();
        $this->employee_repo = $employee_repo;
        self::$data['active_menu'] = 'employees';
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.employees.view', self::$data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        $info = $this->employee_repo->allDataTable($request->all());
        $count =  $this->employee_repo->countDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);


        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.employees.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.employees.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(Request $request)
    {
        $name = $request->get('name');
        $status = (int)$request->get('status');

        $data = [
            'name' => $name,
            'status' => $status
        ];

        $validator = Validator::make($data, [
            'name' => 'required|min:3|max:60|unique:employees,name',
            'status' => 'required|numeric|in:0,1'
        ]);
        /////////////////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////
        $add = $this->employee_repo->store($data);
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
//        Cache::forget('spatie.permission.cache');
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.employees.view'));
        }
        parent::$data['info'] = $info;
        return view('admin.employees.edit', parent::$data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);
        $name = $request->get('name');
        $status = (int)$request->get('status');

        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.employees.view'));
        }

        $data = [
            'name' => $name,
            'status' => $status
        ];
        $validator = Validator::make($request->all(),[
            'name' => 'required|min:3|max:60|unique:employees,name,' . $id,
            'status' => 'required|numeric|in:0,1'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////////////
        $update = $this->employee_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        //////////////////////////////

        $info = $this->employee_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $info->delete();//$this->employee_repo->delete($info);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
}
