<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewAppointmentController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        
        parent::$data['active_menu'] = 'newappointment';
    }
    public function index()
    {
        return view("admin.pages.newappointment", parent::$data);
    }
    public function show ()
    {
        return view('admin.pages.newappointment', [
            'appointment' => Appointment::All(),
        ]);

    }
    
}
