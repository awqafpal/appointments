<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\AppointmentRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class AppointmentsController
 * @property AppointmentRepositoryInterface $appointment_repo
 * @package App\Http\Controllers\Admin
 */
class AppointmentsController extends AdminController
{

    protected $rules  = [
        'tittle'=> 'required|string|min:3|max:255',
        //'palce'=> 'required|string|min:5|max:255',
        'description'=> 'min:5|max:255|string|nullable',
        'date'=> 'required',
//        'type'=> 'required',
        'time_start' => 'required',
        'time_end' => 'required|after:time_start',
        'participant'=> 'nullable',
    ] ;

    /**
     * AppointmentsController constructor.
     * @param AppointmentRepositoryInterface $appointment_repo
     */
    public function __construct(AppointmentRepositoryInterface $appointment_repo)
    {
        parent::__construct();
        $this->appointment_repo = $appointment_repo;
        parent::$data['active_menu'] = 'newappointment';
    }
    /////////////////////////////////////////
    //List All appointment
    public function index ()
    {


        $request = request();
        $tittle = $request->query('tittle');
        $palce = $request->query('palce');
        $date = $request->query('date');
        $time_start = $request->query('time_start');
        $time_end = $request->query('time_end');
//        $type = $request->query('type');
        $archived = 1;

        $perpage = $request->query('perpage');
        if($perpage==null)
        {
            Parent::$data['appointment'] = Appointment::paginate(5);
        }
        elseif ($perpage == 'all')
        {
            parent::$data['appointment'] = Appointment::all();
        }
        else
        {
            parent::$data['appointment']  = Appointment::paginate($perpage);
        }

        parent::$data['tittle'] = $tittle;
        parent::$data['palce'] = $palce;
        parent::$data['date'] = $date;
//        parent::$data['type'] = $type;
        parent::$data['time_start'] = $time_start;
        parent::$data['time_end'] = $time_end;
        parent::$data['archived'] = 1;

        return view('admin.appointments.index', parent::$data);


    }
    public function search(Request $request)
    {

        $tittle = $request->query('tittle');
        $palce = $request->query('palce');
        $date = $request->query('date');
        $time_start = $request->query('time_start');
        $time_end = $request->query('time_end');
        $type = $request->query('type');

        $data = Appointment::when($tittle, function($query, $tittle)
        {
             return $query->where('tittle', 'LIKE', "%$tittle%");

        })

        ->when($palce, function($query, $palce)
        {
             return $query->orWhere('palce', 'LIKE', "%$palce%");

        })

        ->when($date, function($query, $date)
        {
             return $query->orWhere('date', '=', $date);

        })
        ->when($time_start, function($query, $time_start)
        {
             return $query->orWhere('time_start', '>=', $time_start);

        })
        ->when($time_end, function($query, $time_end)
        {
             return $query->orWhere('time_end', '<=', $time_end);

        })
        ->when($type, function($query, $type)
        {
             return $query->orWhere('type', '=', $type);

        })
        ->paginate(5) ;
        parent::$data['appointment'] = $data;
        parent::$data['tittle'] = $tittle;
        parent::$data['palce'] = $palce;
        parent::$data['date'] = $date;
        parent::$data['type'] = $type;
        parent::$data['time_start'] = $time_start;
        parent::$data['time_end'] = $time_end;
        return view('admin.appointments.index', parent::$data);

    }
    // Show create form
    public function create()
    {
        parent::$data['appointment'] = Appointment::all();
        return view('admin.appointments.add', parent::$data);
    }
    // Create appointment on form submit
    public function store(Request $request)
    {

        $request->validate($this->rules ,  [
            'required' => 'هذا الحقل مطلوب',
            'min' => 'هذا الحقل أقل من :min',
            'max' => 'هذا الحقل أكثر من :max',
            'after' => 'يجب أن يكون التوقيت بعد توقيت البداية',
            'unique' => 'ممنوع تكرار التوقيت',
        ]);
        //$request->all()
        $time_start = $request->get('time_start');
        $_time_start = \Carbon\Carbon::createFromFormat('H:i a',$time_start)->format('H:i');
        $time_end = $request->get('time_end');
        $_time_end = \Carbon\Carbon::createFromFormat('H:i a',$time_end)->format('H:i');
        $data = [
            'tittle' => $request->get('tittle'),
            //'palce' => $request->get('palce'),
            'guests' => $request->get('guests'),
            'date' => $request->get('date'),
            'color' => $request->get('color'),
            'time_start' => $_time_start,
            'time_end' => $_time_end,
            'participant' => $request->get('participant'),
        ];
        $appointment = Appointment::create($data);
        return redirect()
        ->route('admin.appointments.add')
        ->with('success', 'تم انشاء الموعد');

    }

     // Show edit form
     public function edit($id)
     {
         $appointment = Appointment::findOrFail($id);
         parent::$data['appointment'] = $appointment;

         return view('admin/appointments/edit',parent::$data);
     }
     // Update appointment on form submit
     public function update(Request $request, $id)
     {
        $request->validate($this->rules ,  [
            'required' => 'هذا الحقل مطلوب',
            'min' => 'هذا الحقل أقل من :min',
            'max' => 'هذا الحقل أكثر من :max',
            'after' => 'يجب أن يكون التوقيت بعد توقيت البداية',
            'unique' => 'ممنوع تكرار التوقيت',


        ]);

        $appointment = Appointment::findOrFail($id);

         $time_start = $request->get('time_start');
         $_time_start = \Carbon\Carbon::createFromFormat('H:i a',$time_start)->format('H:i');
         $time_end = $request->get('time_end');
         $_time_end = \Carbon\Carbon::createFromFormat('H:i a',$time_end)->format('H:i');
         $data = [
             'tittle' => $request->get('tittle'),
             'palce' => $request->get('palce'),
             'guests' => $request->get('guests'),
             'date' => $request->get('date'),
             'color' => $request->get('color'),
             'time_start' => $_time_start,
             'time_end' => $_time_end,
             'participant' => $request->get('participant'),
         ];
        $appointment->update($data);


        return redirect()->route('admin.appointments.view')
        ->with('success', 'تم حفظ التعديل');
        ;
     }
     // Delete appointment
     public function delete($id)
     {
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();

        return redirect(route('admin.index'))
        ->with('success', 'تم حذف الموعد');
     }
     // Aboja3fer

    public function getIndex()
    {
        parent::$data['archived'] = 0;
        return view('admin.appointments.view', parent::$data);
    }
    /////////////////////////////////////////
    //List Archived All appointment
    public function getArchived ()
    {
        parent::$data['archived'] = 1;
        return view('admin.appointments.view', parent::$data);
    }
    /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        $info = $this->appointment_repo->allDataTable($request->all());
        $count =  $this->appointment_repo->countDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);

        $dataTable->editColumn('date', function ($row)
        {
            Carbon::setLocale('ar');
            $date = Carbon::create($row->date);
            return $date->format('Y-m-d') . ' ' . Common::getArabicDay($date);
        });
        $dataTable->editColumn('status', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['status'] = $row->status;

            return view('admin.appointments.parts.status', $data)->render();
        });
        $dataTable->editColumn('color', function ($row)
        {
            $date = Carbon::create($row->date);
            return parent::$data['colors'][$date->dayOfWeek];
        });

        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['archived'] = $row->archived;

            return view('admin.appointments.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*'])->addIndexColumn();
        return $dataTable->make(true);
    }
    /*
    public function getIndex()
    {
        parent::$data['appointment'] = Appointment::all();
        return view('admin.appointments.index', parent::$data);

    }
    */
    public function getAdd()
    {
        return view('admin.appointments.add', parent::$data);

    }
    public function postAdd()
    {
        // code to add appointment
    }
    public function getEdit(Request $request, $id)
    {
        return view('admin.appointments.add', parent::$data);

    }
    public function postEdit(Request $request, $id)
    {
        // code to edit appointment
    }
    public function getDetails(Request $request, $id)
    {
        $appointment = Appointment::findOrFail($id);
         parent::$data['appointment'] = $appointment;

         return view('admin.appointments.details',parent::$data);


        //return view('admin.appointments.details', parent::$data);

    }
    /**
     * @param Request $request
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdateStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $info = $this->appointment_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }

        $data = [
            'status' => $status
        ];
        $validator = Validator::make($request->all(),[
            'status' => 'required|in:0,1'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////////////
        $update = $this->appointment_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /**
     * @param Request $request
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postArchive(Request $request,$id)
    {
//        $id = $request->get('id');
        $info = $this->appointment_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }
        $data = [
            'archived' => 1
        ];
//        $validator = Validator::make($request->all(),[
//            'archived' => 'required|in:0,1'
//        ]);
//        ////////////////////////
//        if ($validator->fails())
//        {
//            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
//        }
        //////////////////////////////////////////////
        $update = $this->appointment_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /**
     * @param Request $request
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUnArchive(Request $request,$id)
    {
//        $id = $request->get('id');
        $info = $this->appointment_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }
        $data = [
            'archived' => 0
        ];
        //////////////////////////////////////////////
        $update = $this->appointment_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        //////////////////////////////

        $info = Appointment::find($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $info->delete();//$this->employee_repo->delete($info);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
}
