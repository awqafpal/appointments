<?php

namespace App\Http\Controllers\Admin;

use App\appointment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class appointmentController extends Controller
{
    //List All appointment
    public function index ()
    {
        return view('admin.pages.newappointment', [
            'appointment' => appointment::all(),
        ]);

    }
    // Show create form
    public function create()
    {
        # code...
    }
    // Create appointment on form submit
    public function store(Request $request)
    {
        $appointment = new Appointment();
        $appointment->tittle = $request->post('tittle');
        $appointment->palce = $request->post('palce');
        $appointment->type = $request->post('type');
        $appointment->description = $request->post('description');
        $appointment->case = $request->post('case');
        $appointment->guests = $request->post('guests');
        $appointment->date = $request->post('date');
        $appointment->date_end = $request->post('date_end');
        $appointment->time = $request->post('time');
        $appointment->time_end = $request->post('time_end');

        return $appointment;
    }

     // Show edit form
     public function edit($id)
     {
         # code...
     }
     // Update appointment on form submit
     //public function update(Request $request $id)
    // {
         # code...
     //}
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        //////////////////////////////

        $info = Appointment::find($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $info->delete();//$this->employee_repo->delete($info);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
}
