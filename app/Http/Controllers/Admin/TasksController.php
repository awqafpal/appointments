<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FaqController
 * @property TaskRepositoryInterface $task_repo
 * @property EmployeeRepositoryInterface $employee_repo
 * @package App\Http\Controllers\Admin
 */
class TasksController extends AdminController
{
    /**
     * @var TaskRepositoryInterface
     */
    protected $task_repo;
    /**
     * @var EmployeeRepositoryInterface
     */
    protected $employee_repo;
    /**
     * FaqController constructor.
     * @param TaskRepositoryInterface $task_repo
     */
    public function __construct(TaskRepositoryInterface $task_repo,EmployeeRepositoryInterface $employee_repo)
    {
        parent::__construct();
        $this->task_repo = $task_repo;
        $this->employee_repo = $employee_repo;
        self::$data['active_menu'] = 'tasks';
        self::$data['task_status'] = [
            'pending' => 'قيد التنفيذ',
            'suspended' => 'عالق',
            'done' => 'تم',
        ];
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.tasks.view', self::$data);
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getArchive()
    {
        return view('admin.tasks.archive', self::$data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        $info = $this->task_repo->allUnFinishedDataTable($request->all());
        $count =  $this->task_repo->countUnFinishedDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);

        $dataTable->editColumn('employees', function ($row)
        {
            $employees = '';
            foreach ($row->employees as $employee){
                $employees .= $employee->name . ',';
            }
            return $employees;
        });

        $dataTable->editColumn('status', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['status'] = $row->status;
            $data['task_status'] = self::$data['task_status'];

            return view('admin.tasks.parts.status', $data)->render();
        });

        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.tasks.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }
    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getArchiveList(Request $request)
    {
        $info = $this->task_repo->allDataTable($request->all());
        $count =  $this->task_repo->countDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);

        $dataTable->editColumn('employees', function ($row)
        {
            $employees = '';
            foreach ($row->employees as $employee){
                $employees .= $employee->name . ',';
            }
            return $employees;
        });

        $dataTable->editColumn('archive_status', function ($row)
        {
            $data['id_hash'] = $row->id_hash;
            $data['status'] = $row->status;
            $data['task_status'] = self::$data['task_status'];

            return view('admin.tasks.parts.archive_status', $data)->render();
        });

        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.tasks.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        parent::$data['employees'] = $this->employee_repo->all();
        return view('admin.tasks.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(Request $request)
    {
        $name = $request->get('name');
        $status = $request->get('status');
        $employees = $request->get('employees');
        $data = [
            'name' => $name,
            'status' => $status
        ];
        $validator = Validator::make($data, [
            'name' => 'required',
            'status' => 'required|in:pending,suspended,done'
        ]);
        /////////////////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////
        $add = $this->task_repo->store($data);
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        $add->employees()->sync($employees);
//        Cache::forget('spatie.permission.cache');
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->task_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }
        parent::$data['info'] = $info;
        parent::$data['employees'] = $this->employee_repo->all();
        return view('admin.tasks.edit', parent::$data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);
        $name = $request->get('name');
        $status = $request->get('status');
        $employees = $request->get('employees');
        $info = $this->task_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }

        $data = [
            'name' => $name,
            'status' => $status
        ];
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'status' => 'required|in:pending,suspended,done'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////////////
        $update = $this->task_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        $info->employees()->sync($employees);
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /**
     * @param Request $request
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdateStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $info = $this->task_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.tasks.view'));
        }

        $data = [
            'status' => $status
        ];
        $validator = Validator::make($request->all(),[
            'status' => 'required|in:pending,suspended,done'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////////////
        $update = $this->task_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        //////////////////////////////

        $info = $this->task_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        $delete = $info->delete();//$this->employee_repo->delete($info);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
}
