<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attachments;
use App\Models\BusinessSchedule;
use App\Repositories\Interfaces\BusinessScheduleRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class FaqController
 * @property BusinessScheduleRepositoryInterface $business_schedule_repo
 * @package App\Http\Controllers\Admin
 */
class BusinessScheduleController extends AdminController
{
    /**
     * @var BusinessScheduleRepositoryInterface
     */
    protected $business_schedule_repo;
    /**
     * FaqController constructor.
     * @param BusinessScheduleRepositoryInterface $business_schedule_repo
     */
    public function __construct(BusinessScheduleRepositoryInterface $business_schedule_repo)
    {
        parent::__construct();
        $this->business_schedule_repo = $business_schedule_repo;
        self::$data['active_menu'] = 'business_schedules';
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.business_schedules.view', self::$data);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getList(Request $request)
    {
        $info = $this->business_schedule_repo->allDataTable($request->all());
        $count =  $this->business_schedule_repo->countDataTable($request->all());
        $dataTable = Datatables::of($info)->setTotalRecords($count);


        $dataTable->addColumn('attachments', function ($row)
        {
            $data['attachments'] = $row->attachments;
            return view('admin.business_schedules.parts.attachments', $data)->render();
        });
        $dataTable->addColumn('actions', function ($row)
        {
            $data['id_hash'] = $row->id_hash;

            return view('admin.business_schedules.parts.actions', $data)->render();
        });
        $dataTable->escapeColumns(['*']);
        return $dataTable->make(true);
    }
    /////////////////////////////////////////
    public function getAdd()
    {
        return view('admin.business_schedules.add', parent::$data);
    }
    /////////////////////////////////////////
    public function postAdd(Request $request)
    {
        $title = $request->get('title');
        $placement_party = $request->get('placement_party');
        $project = $request->get('project');
        $notes = $request->get('notes');
        $status = (int)$request->get('status');

        $data = [
            'title' => $title,
            'placement_party' => $placement_party,
            'project' => $project,
            'notes' => $notes,
            'status' => $status
        ];

        $validator = Validator::make($data, [
            'title' => 'required',
            'placement_party' => 'required',
            'project' => 'nullable',
            'notes' => 'nullable',
            'status' => 'required|numeric|in:0,1'
        ]);
        /////////////////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////
        $add = $this->business_schedule_repo->store($data);
        if (!$add)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
//        Cache::forget('spatie.permission.cache');
        //////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.added'),$add);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);

        $info = $this->business_schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.business_schedules.view'));
        }
        parent::$data['info'] = $info;
        return view('admin.business_schedules.edit', parent::$data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit(Request $request, $id)
    {
        $id = $this->decrypt($id);
        $title = $request->get('title');
        $placement_party = $request->get('placement_party');
        $project = $request->get('project');
        $notes = $request->get('notes');
        $status = (int)$request->get('status');

        $info = $this->business_schedule_repo->get($id);
        if (!$info)
        {
            $request->session()->flash('data', ['title' => trans('title.info'),  'code' => 300, 'message' => trans('messages.not_found')]);
            return redirect(route('admin.business_schedules.view'));
        }

        $data = [
            'title' => $title,
            'placement_party' => $placement_party,
            'project' => $project,
            'notes' => $notes,
            'status' => $status
        ];
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'placement_party' => 'required',
            'project' => 'nullable',
            'notes' => 'nullable',
            'status' => 'required|numeric|in:0,1'
        ]);
        ////////////////////////
        if ($validator->fails())
        {
            return $this->generalResponse('false',400, trans('title.warning'), $validator->messages()->first(),null);
        }
        //////////////////////////////////////////////
        $update = $this->business_schedule_repo->update($id, $data);
        if (!$update)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        //////////////////////////////////////////////
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'),$update);
    }
    /////////////////////////////////////////
    public function getDelete(Request $request, $id)
    {
        //////////////////////////////

        $info = $this->business_schedule_repo->get($id);
        if (!$info)
        {
            return $this->generalResponse('false',300, trans('title.info'), trans('messages.not_found'),null);
        }
        foreach ($info->attachments as $att){
            try {
                unlink(public_path($att->name));
            }
            catch (\Exception $ex){

            }
            $att->delete();
        }
        $delete = $info->delete();//$this->business_schedule_repo->delete($info);
        if (!$delete)
        {
            return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
        }
        return $this->generalResponse('true',200, trans('title.success'), trans('messages.deleted'),null);
    }
    public function getOrder()
    {
        self::$data['business_schedule_items'] = $this->business_schedule_repo->all();
        return view('admin.business_schedules.order', self::$data);
    }
    public function postOrder(Request $request)
    {
        $ids = $request->ids;
        if(is_array($ids)){
            $this->business_schedule_repo->updateOrder($ids);
            return $this->generalResponse('true',200, trans('title.success'), trans('messages.updated'));
        }
        return $this->generalResponse('false',500, trans('title.error'), trans('messages.error'),null);
    }
    public function postUpload(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');

            $data = [];
            $business_schedule = BusinessSchedule::find($id);
            if (!$business_schedule) {
                return $this->generalResponse('false', 300, trans('title.info'), trans('messages.not_found'), null);
            }
            $year = date('Y');
            $month = date('m');
            $path = 'uploads/' . $year . '/' . $month . '/';

            if (!file_exists(public_path($path))) {
                mkdir(public_path($path), 0755, true);
            }
            foreach ($files as $k => $myFile) {

                $image = 'image_' . rand(1, 10000) . '_' . strtotime(date("Y-m-d H:i:s")) . '.' . $myFile->getClientOriginalExtension();
                $myFile->move($path, $image);

                $file = new Attachments();

                $file->title = $myFile->getClientOriginalName();
                $file->name = $path . $image;
                $file->business_schedule_id = $id;
                $saved = $file->save();

                if ($saved) {
                    $data[] = [
                        'id' => $file->id,
                        'url' => $file->name
                    ];
//                return response()->json(['status' => true, 'message' => 'Image(s) Uploaded.']);
                }
            }
            return $this->generalResponse('true', 200, trans('title.success'), trans('messages.updated'), $data);
        }
    }

    public function postUploadSingleImage(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $myFile = $request->file('file');
            $business_schedule = BusinessSchedule::find($id);
            if (!$business_schedule) {
                return $this->generalResponse('false', 300, trans('title.info'), trans('messages.not_found'), null);
            }
            $year = date('Y');
            $month = date('m');
            $path = 'uploads/' . $year . '/' . $month . '/';

            if (!file_exists(public_path($path))) {
                mkdir(public_path($path), 0755, true);
            }
            $image = 'image_' . rand(1, 10000) . '_' . strtotime(date("Y-m-d H:i:s")) . '.' . $myFile->getClientOriginalExtension();
            $myFile->move($path, $image);


            $file = new Attachments();

            $file->title = $myFile->getClientOriginalName();
            $file->name = $path . $image;
            $file->business_schedule_id = $id;
            $saved = $file->save();

            if ($saved) {
                $data = [
                    'id' => $file->id,
                    'url' => $file->name
                ];
            }
            return $this->generalResponse('true', 200, trans('title.success'), trans('messages.updated'), $data);
        }
    }

    public function postAttachmentDelete(Request $request, $id)
    {
        $image = Attachments::find($id);
        if (!$image) {
            return $this->generalResponse('false', 300, trans('title.info'), trans('messages.not_found'), null);
        }
        $path = $image->name;
        $image->delete();
        try {
            unlink($path);
        } catch (\Exception $e) {

        }
        return $this->generalResponse('true', 200, trans('title.success'), trans('messages.deleted'), null);
    }

}
