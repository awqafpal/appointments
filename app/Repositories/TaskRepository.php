<?php


namespace App\Repositories;
use App\Models\Task;
use App\Repositories\Interfaces\TaskRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class TaskRepository
 * @property Task $task
 * @package App\Repositories
 */
class TaskRepository implements TaskRepositoryInterface
{
    /**
     * TaskRepository constructor.
     */
    function __construct()
    {
        $this->task = new Task();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->task->find($id);
    }
    /**
     * Get's all tasks.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->task->all();
    }

    /**
     * Deletes a task.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->task->where('id', $id)->delete();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->task->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $updated = $this->task->find($id)->update($data);
        if($updated){
            return $this->task;
        }
        return $updated;
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allUnFinishedDataTable(array $data)
    {
        $query = $this->task->with('employees')->where('status', '<>', 'done');
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"start") && !is_null($data['start']))
        {
            $skip = $data['start'];
        }
        if(Arr::exists($data,"length") && !is_null($data['length']))
        {
            $take = $data['length'];
        }
        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countUnFinishedDataTable(array $data)
    {
        $query = $this->task->where('status', '<>', 'done');

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        return $query->count('id');
    }
    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->task->with('employees');
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"start") && !is_null($data['start']))
        {
            $skip = $data['start'];
        }
        if(Arr::exists($data,"length") && !is_null($data['length']))
        {
            $take = $data['length'];
        }
        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->task;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        return $query->count('id');
    }

}

