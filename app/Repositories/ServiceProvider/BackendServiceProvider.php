<?php
namespace App\Repositories\ServiceProvider;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Interfaces\UserRepositoryInterface',
            'App\Repositories\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\RoleRepositoryInterface',
            'App\Repositories\RoleRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\AppointmentRepositoryInterface',
            'App\Repositories\AppointmentRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\EmployeeRepositoryInterface',
            'App\Repositories\EmployeeRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\TaskRepositoryInterface',
            'App\Repositories\TaskRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\BusinessScheduleRepositoryInterface',
            'App\Repositories\BusinessScheduleRepository'
        );
    }
}
