<?php


namespace App\Repositories;
use App\Models\BusinessSchedule;
use App\Repositories\Interfaces\BusinessScheduleRepositoryInterface;
use Illuminate\Support\Arr;

/**
 * Class BusinessScheduleRepository
 * @property BusinessSchedule $business_schedule
 * @package App\Repositories
 */
class BusinessScheduleRepository implements BusinessScheduleRepositoryInterface
{
    /**
     * BusinessScheduleRepository constructor.
     */
    function __construct()
    {
        $this->business_schedule = new BusinessSchedule();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->business_schedule->with(['attachments'])->find($id);
    }
    /**
     * Get's all business_schedules.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->business_schedule->orderBy('sort')->get();
    }

    /**
     * Deletes a business_schedule.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
//        dd($id);
        return $this->business_schedule->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->business_schedule->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->business_schedule->find($id)->update($data);
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->business_schedule;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"start") && !is_null($data['start']))
        {
            $skip = $data['start'];
        }
        if(Arr::exists($data,"length") && !is_null($data['length']))
        {
            $take = $data['length'];
        }
        return $query->orderBy('sort')->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->business_schedule;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('name', 'LIKE', '%' . $data['name']. '%');
        }
        return $query->count('id');
    }
    /**
     * @param array $ids
     * @return mixed
     */
    public function updateOrder($ids)
    {
        foreach ($ids as $k => $id){
            $business_schedule = $this->business_schedule->find($id);//->update([ 'sort', ($k + 1)]);
            $business_schedule->sort = $k + 1;
            $business_schedule->save();
        }
        return 1;
    }
}

