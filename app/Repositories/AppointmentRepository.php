<?php


namespace App\Repositories;
use App\Appointment;
use App\Repositories\Interfaces\AppointmentRepositoryInterface;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;

/**
 * Class AppointmentRepository
 * @property Role $appointment
 * @package App\Repositories
 */
class AppointmentRepository implements AppointmentRepositoryInterface
{
    /**
     * AppointmentRepository constructor.
     */
    function __construct()
    {
        $this->appointment = new Appointment();
    }
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->appointment->find($id);
    }
    /**
     * Get's all appointments.
     *
     * @return mixed
     */
    public function all()
    {
        return $this->appointment->all();
    }

    /**
     * Deletes a appointment.
     *
     * @param int
     * @return int
     */
    public function delete($id)
    {
        return $this->appointment->destroy($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->appointment->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->appointment->find($id)->update($data);
    }

    /**
     * @param array $data
     * @return $this|\Illuminate\Database\Eloquent\Builder|mixed
     */
    public function allDataTable(array $data)
    {
        $query = $this->appointment;
        $skip = 0;
        $take = 25;

        if(Arr::exists($data,"archived") && !is_null($data['archived']))
        {
            $query = $query->where('archived', $data['archived']);
        }
        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('tittle', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"from") && !is_null($data['from']))
        {
            $query = $query->where('date', '>=', $data['from']);
        }
        if(Arr::exists($data,"to") && !is_null($data['to']))
        {
            $query = $query->where('date', '<=', $data['to']);
        }
        if(Arr::exists($data,"start") && !is_null($data['start']))
        {
            $skip = $data['start'];
        }
        if(Arr::exists($data,"length") && !is_null($data['length']))
        {
            $take = $data['length'];
        }
        return $query->skip($skip)->take($take);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data)
    {
        $query = $this->appointment;

        if(Arr::exists($data,"name") && !is_null($data['name']))
        {
            $query = $query->where('tittle', 'LIKE', '%' . $data['name']. '%');
        }
        if(Arr::exists($data,"from") && !is_null($data['from']))
        {
            $query = $query->where('date', '>=', $data['from']);
        }
        if(Arr::exists($data,"to") && !is_null($data['to']))
        {
            $query = $query->where('date', '<=', $data['to']);
        }
        if(Arr::exists($data,"start") && !is_null($data['start']))
        {
            $skip = $data['start'];
        }
        if(Arr::exists($data,"length") && !is_null($data['length']))
        {
            $take = $data['length'];
        }
        return $query->count('id');
    }

}

