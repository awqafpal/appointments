<?php


namespace App\Repositories\Interfaces;
/**
 * Interface BusinessScheduleRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface BusinessScheduleRepositoryInterface
{
    /**
     * Get's a business_schedule by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all business_schedules.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a business_schedule.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
}
