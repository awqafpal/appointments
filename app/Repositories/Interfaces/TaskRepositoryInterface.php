<?php


namespace App\Repositories\Interfaces;
/**
 * Interface TaskRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface TaskRepositoryInterface
{
    /**
     * Get's a role by it's ID
     *
     * @param int
     */
    public function get($id);

    /**
     * Get's all roles.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a role.
     *
     * @param int
     */
    public function delete($id);

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data);


    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    public function allDataTable(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function countDataTable(array $data);
    public function allUnFinishedDataTable(array $data);
    public function countUnFinishedDataTable(array $data);
}
