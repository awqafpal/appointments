<?php


namespace App\Helpers;


class Common
{
    public static function getMenu($user){
        $menu = [];
        $user_permissions = $user->getAllPermissions();
        $menu = $user_permissions->where('parent_id', 0);
        foreach ($menu as $k => $menu_item){
            $menu[$k]['children'] = $user_permissions->where('parent_id', $menu_item->id);
        }
        return $menu;
    }
    /////////////////////////////////////////
    public static function getArabicDay($date)
    {
        $days = [
            "Sat" => "السبت",
            "Sun" => "الأحد",
            "Mon" => "الإثنين",
            "Tue" => "الثلاثاء",
            "Wed" => "الأربعاء",
            "Thu" => "الخميس",
            "Fri" => "الجمعة"
        ];
        $day = $date->format("D");
        $day = $days[$day];
        return $day;
    }
}
