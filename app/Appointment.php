<?php

namespace App;

use App\Models\BaseModel;
use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;

class Appointment extends BaseModel
{
    use EncryptionTrait;
    protected $table = "appointment";
    protected $fillable = ['id', 'tittle', 'palce', 'type','description', 'guests', 'date', 'time_start', 'time_end', 'participant', 'color', 'status', 'archived'];
}
