<?php

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;

class BusinessSchedule extends BaseModel
{
    use EncryptionTrait;
    protected $table = "business_schedule";
    protected $fillable = ['title', 'placement_party', 'project', 'notes', 'sort', 'status'];
    public function attachments(){
        return $this->hasMany('App\Models\Attachments', 'business_schedule_id', 'id');
    }
}
