<?php

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;

class Attachments extends BaseModel
{
    use EncryptionTrait;
    protected $table = "attachments";
    protected $fillable = ['title', 'name', 'business_schedule_id'];

    public function schedule(){
        return $this->belongsTo('App\Models\BusinessSchedule', 'business_schedule_id', 'id');
    }
}
