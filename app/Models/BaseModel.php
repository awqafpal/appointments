<?php
/**
 * Created by PhpStorm.
 * User: Redwan-PC
 * Date: 12/16/2018
 * Time: 3:30 PM
 */

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BaseModel extends Model
{
    use EncryptionTrait;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    function getIdHashAttribute()
    {
        return $this->encrypt($this->id);
    }
    /////////////////////////////////////////////////////////
    public function getHumansDateAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    public static function getHumansDate($created_at)
    {
        return Carbon::parse($created_at)->diffForHumans();
    }
    /////////////////////////////////////////
    public static function getArabicMonth($date)
    {
        $months = [
            "Jan" => "يناير",
            "Feb" => "فبراير",
            "Mar" => "مارس",
            "Apr" => "ابريل",
            "May" => "مايو",
            "Jun" => "يونيو",
            "Jul" => "يوليو",
            "Aug" => "أغسطس",
            "Sep" => "سبتمبر",
            "Oct" => "أكتوبر",
            "Nov" => "نوفمبر",
            "Dec" => "ديسمبر"
        ];

        $month = date("M", strtotime($date));

        $month = $months[$month];
        return $month;
    }
}
