<?php

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;

class Employee extends BaseModel
{
    use EncryptionTrait;
    protected $table = "employees";
    protected $fillable = ['name', 'mobile'];
    public function tasks()
    {
        return $this->belongsToMany('App\Models\Task', 'task_employees', 'employee_id', 'task_id');
    }
}
