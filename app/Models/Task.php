<?php

namespace App\Models;

use App\Traits\EncryptionTrait;
use Illuminate\Database\Eloquent\Model;

class Task extends BaseModel
{
    use EncryptionTrait;
    protected $table = "tasks";
    protected $fillable = ['name', 'status'];
    public function employees()
    {
        return $this->belongsToMany('App\Models\Employee', 'task_employees', 'task_id', 'employee_id');
    }
}
